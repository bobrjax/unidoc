from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, re_path
from django.views.generic.base import RedirectView

from Teta import settings
from main import views
from main.views import *

favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', index),
    path('help/', help),
    path('user/', user),
    path('user/<int:rid>', views.document_delete, name='document_delete'),
    path('new/', new),
    path('change/', views.change_doc, name='change_doc'),
    path('change/success', views.document_change, name='document_change'),
    path('', index),
    path('orders/<int:rid>', views.document_reset, name='document_reset'),
    path('orders/', orders),
    path('service_notes/', service_notes),
    path('service_notes/<int:rid>', views.document_reset, name='document_reset'),
    path('instructions/', instructions),
    path('instructions/<int:rid>', views.document_reset, name='document_reset'),
    path('file_storage/file_storage/<str:link>', pdf_view),
    re_path(r'^favicon\.ico$', favicon_view),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Панель администратора"
admin.site.index_title = "UniDoc"
admin.site.site_title = "Панель администратора"

handler404 = pageNotFound
