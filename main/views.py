from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import FileResponse
from django.shortcuts import render, redirect

from main.forms import LoginForm
from main.models import Documents, Groups
#from django.forms import FileUploadForm

def index(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/user/")
            else:
                return redirect("/")
    else:
        if request.user.is_authenticated:
            logout(request)
        form = LoginForm()
        return render(request, 'new_index.html', {'form': form})


@login_required(login_url="/")
def help(request):
    return render(request, 'help.html')

@login_required(login_url="/")
def pdf_view(request, link):
    try:
        document = Documents.objects.get(file_link='file_storage/'+link)
    except:
        return pageNotFound(request, 0)
    if request.user.level <= document.acl:
        try:
            return FileResponse(open('file_storage/file_storage/' + link, 'rb'), content_type='application/pdf')
        except:
            return pageNotFound(request, 0)
    else:
        return pageNotFound(request, 0)

@login_required(login_url="/")
def service_notes(request):
    return render(request, 'service_notes.html',
                  {'Documents': Documents.objects.filter(type='r', acl__gte=request.user.level).select_related('uid')})

@login_required(login_url="/")
def new(request):
    if request.method == 'POST':
        print(request.POST)
        print(request.FILES)
        Documents.objects.create(
            uid_id=request.user.id,
            name=request.POST.get('name'),
            type=request.POST.get('type'),
            file_link=request.FILES.get('inputfile'),
            acl=request.user.level
        )
        return render(request, 'create.html')
    else:
        return render(request, 'create.html')


def change_doc(request):
    return render(request, 'change.html',
                  {'Documents': Documents.objects.filter(reset=1).select_related('uid')})

def document_change(request):
    if request.method == 'POST':
        print(request.POST)
        print(request.FILES)
        print(request.POST.get('doc'))
        document = Documents.objects.get(id=request.POST.get('doc'))
        document.acl = request.user.level
        document.file_link = request.FILES.get('inputfile')
        document.reset = 0
        document.save()
        return render(request, 'change.html',
                  {'Documents': Documents.objects.filter(reset=1).select_related('uid')})
    else:
        return render(request, 'change.html',
                  {'Documents': Documents.objects.filter(reset=1).select_related('uid')})

@login_required(login_url="/")
def orders(request):
     return render(request, 'orders.html',
                  {'Documents': Documents.objects.filter(type='o', acl__gte=request.user.level).select_related('uid')})

@login_required(login_url="/")
def user(request):
    return render(request, 'user.html',
                  {'Documents': Documents.objects.filter(uid=request.user.id)})


@login_required(login_url="/")
def document_delete(request, rid):
    current_url = request.path
    if request.method == 'POST':
        document = Documents.objects.get(id=rid)
        if document.uid_id == request.user.id:
            document.delete()
    return render(request, 'user.html',
                  {'Documents': Documents.objects.filter(uid=request.user.id)})

@login_required(login_url="/")
def document_reset(request, rid):
    if request.method == 'POST':
        document = Documents.objects.get(id=rid)
        document.file_link = "empty"
        document.acl = 1
        document.reset = 1
        document.save()
    return render(request, 'user.html',
                  {'Documents': Documents.objects.filter(uid=request.user.id)})

@login_required(login_url="/")
def instructions(request):
     return render(request, 'instructions.html',
              {'Documents': Documents.objects.filter(type='i', acl__gte=request.user.level).select_related('uid')})


def pageNotFound(request, exception):
    if request.user.is_authenticated:
        return redirect('/user/', permanent=True)
    else:
        return redirect('/', permanent=True)

