# Generated by Django 4.2.6 on 2023-10-22 08:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_alter_users_options_alter_users_managers_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='is_enforce_group',
        ),
        migrations.RemoveField(
            model_name='users',
            name='login',
        ),
        migrations.AlterField(
            model_name='users',
            name='level',
            field=models.IntegerField(default=1),
        ),
    ]
