# Generated by Django 4.2.6 on 2023-10-23 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_alter_users_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='last_login',
            field=models.DateTimeField(auto_now=True, default='2023-10-22 08:32:45.8874590 +00:00'),
            preserve_default=False,
        ),
    ]
