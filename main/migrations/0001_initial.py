# Generated by Django 4.2.6 on 2023-10-13 12:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Documents',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('uid', models.IntegerField()),
                ('time_create', models.DateTimeField(auto_now_add=True)),
                ('file_link', models.FileField(upload_to='file_storage')),
            ],
        ),
        migrations.CreateModel(
            name='Groups',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('last_name', models.CharField(max_length=256)),
                ('login', models.CharField(max_length=256)),
                ('password', models.CharField(max_length=256)),
                ('level', models.IntegerField(default=0)),
                ('group_id', models.IntegerField(default=1)),
                ('is_enforce_group', models.BooleanField(default=False)),
            ],
        ),
    ]
