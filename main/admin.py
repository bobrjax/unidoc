from django.contrib import admin
from . import models

@admin.register(models.Users)
class AuthUserAdmin(admin.ModelAdmin):
    list_display = ('id','username','level')

@admin.register(models.Groups)
class AuthUserAdmin(admin.ModelAdmin):
    list_display = ('id','name')