from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30, label="",
                               widget=forms.TextInput(attrs={"class": "input-login input", 'placeholder': 'ЛОГИН'}))

    password = forms.CharField(max_length=30, label="",
                               widget=forms.PasswordInput(attrs={"class": "input-login input", 'placeholder': 'ПАРОЛЬ'}))