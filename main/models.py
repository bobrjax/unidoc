from django.db import models
from django.contrib.auth.models import AbstractUser


class Groups(models.Model):
    name = models.CharField(max_length=256)


class Users(AbstractUser):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    password = models.CharField(max_length=256)
    level = models.IntegerField(default=1)
    group_id = models.ForeignKey(Groups, on_delete=models.SET_DEFAULT, default=1)

    def save(self, *args, **kwargs):
        if self.password:
            self.set_password(self.password)
        super().save(*args, **kwargs)


@property
def is_authentificated(self):
    return True


class Documents(models.Model):
    name = models.CharField(max_length=256)
    uid = models.ForeignKey(Users, on_delete=models.CASCADE)
    type = models.CharField(max_length=1)
    time_create = models.DateTimeField(auto_now_add=True)
    file_link = models.FileField(upload_to='file_storage/')
    acl = models.IntegerField(default=3)
    reset = models.IntegerField(default=0)